angular.module('starter.controllers', [])

  .controller('DashCtrl', function ($scope) { })

  .controller('ChatsCtrl', function ($scope, Chats) {

    Chats.all()
      .then(function (chats) {
        $scope.chats = chats.rows;
      });

    $scope.remove = function (doc) {
      Chats.remove(doc._id)
        .then(function (doc) {
          console.log('removed', doc);
        })
    }

  })

  .controller('ChatDetailCtrl', function ($scope, $stateParams, Chats) {
    Chats.get($stateParams.chatId)
      .then(function (result) {
        $scope.chat = result;
      })

  })

  .controller('AccountCtrl', function ($scope) {
    $scope.settings = {
      enableFriends: true
    };
  });
