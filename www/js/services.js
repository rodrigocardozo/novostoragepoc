angular.module('starter.services', [])

  .factory('Chats', function () {

    var db = new PouchDB('medsoft-pro');

    var chats = [{
      name: 'Ben Sparrow',
      lastText: 'You on your way?',
      face: 'img/ben.png'
    }, {
      name: 'Max Lynx',
      lastText: 'Hey, it\'s me',
      face: 'img/max.png'
    }, {
      name: 'Adam Bradleyson',
      lastText: 'I should buy a boat',
      face: 'img/adam.jpg'
    }, {
      name: 'Perry Governor',
      lastText: 'Look at my mukluks!',
      face: 'img/perry.png'
    }, {
      name: 'Mike Harrington',
      lastText: 'This is wicked good ice cream.',
      face: 'img/mike.png'
    }];

    function _init() {
      db.bulkDocs(chats).catch(function (err) { console.log('created:error', err); });
    }

    function _all() {
      return db.allDocs({
        include_docs: true,
        attachments: true
      });
    }

    function _get(chat) {
      return db.get(chat);
    }

    function _add(chat) {
      return db.post(chat);
    }

    function _remove(chat) {
      return db.get(chat)
        .then(function (doc) {
          return db.remove(doc);
        });
    }

    return {
      init: _init,
      all: _all,
      remove: _remove,
      get: _get,
      add: _add
    };

  });
